EESchema Schematic File Version 4
LIBS:pqws-main-hw-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "PQWS Main board - Data logging"
Date "2017-12-05"
Rev ""
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4700 3000 0    60   Input ~ 0
CMD
Text HLabel 4700 3200 0    60   Input ~ 0
CLK
Text HLabel 4700 3400 0    60   Input ~ 0
D0
Text HLabel 4700 3500 0    60   Input ~ 0
D1
Text HLabel 4700 2800 0    60   Input ~ 0
D2
Text HLabel 4700 2900 0    60   Input ~ 0
D3
$Comp
L pqws-main-hw-rescue:Micro_SD_Card_Det_Hirose_DM3AT-conn J1
U 1 1 5A286C02
P 5800 3200
F 0 "J1" H 5150 3900 50  0000 C CNN
F 1 "Micro_SD_Card_Det_Hirose_DM3AT" H 6350 2450 50  0000 R CNN
F 2 "Connector_Card:microSD_HC_Hirose_DM3AT-SF-PEJM5" H 7850 3900 50  0001 C CNN
F 3 "" H 5800 3300 50  0001 C CNN
	1    5800 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2800 4700 2800
Wire Wire Line
	4700 2900 4900 2900
Wire Wire Line
	4900 3000 4700 3000
Wire Wire Line
	4700 3200 4900 3200
Wire Wire Line
	4900 3400 4700 3400
Wire Wire Line
	4700 3500 4900 3500
$Comp
L power:GND #PWR028
U 1 1 5A286D1C
P 4200 3300
F 0 "#PWR028" H 4200 3050 50  0001 C CNN
F 1 "GND" H 4200 3150 50  0000 C CNN
F 2 "" H 4200 3300 50  0001 C CNN
F 3 "" H 4200 3300 50  0001 C CNN
	1    4200 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3100 4200 3000
Wire Wire Line
	4900 3100 4200 3100
Wire Wire Line
	4200 3300 4900 3300
$Comp
L power:GND #PWR029
U 1 1 5A286D53
P 4750 3800
F 0 "#PWR029" H 4750 3550 50  0001 C CNN
F 1 "GND" H 4750 3650 50  0000 C CNN
F 2 "" H 4750 3800 50  0001 C CNN
F 3 "" H 4750 3800 50  0001 C CNN
	1    4750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3700 4750 3700
Wire Wire Line
	4750 3700 4750 3800
$Comp
L power:GND #PWR030
U 1 1 5A286D89
P 6600 3700
F 0 "#PWR030" H 6600 3450 50  0001 C CNN
F 1 "GND" H 6600 3550 50  0000 C CNN
F 2 "" H 6600 3700 50  0001 C CNN
F 3 "" H 6600 3700 50  0001 C CNN
	1    6600 3700
	1    0    0    -1  
$EndComp
Text HLabel 4700 3600 0    60   Input ~ 0
SD_DET
Wire Wire Line
	4700 3600 4900 3600
$Comp
L power:VCC #PWR031
U 1 1 5A85AC75
P 4200 3000
F 0 "#PWR031" H 4200 2850 50  0001 C CNN
F 1 "VCC" H 4217 3173 50  0000 C CNN
F 2 "" H 4200 3000 50  0001 C CNN
F 3 "" H 4200 3000 50  0001 C CNN
	1    4200 3000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
