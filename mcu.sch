EESchema Schematic File Version 4
LIBS:pqws-main-hw-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 5 6
Title "PQWS Main board - MCU"
Date "2017-12-05"
Rev ""
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32:STM32L476RGTx U1
U 1 1 5A286040
P 8400 5550
F 0 "U1" H 2400 7575 50  0000 L BNN
F 1 "STM32L476RGTx" H 14400 7575 50  0000 R BNN
F 2 "Package_QFP:LQFP-64_10x10mm_P0.5mm" H 14400 7525 50  0001 R TNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/c5/ed/2f/60/aa/79/42/0b/DM00108832.pdf/files/DM00108832.pdf/jcr:content/translations/en.DM00108832.pdf" H 8400 5550 50  0001 C CNN
	1    8400 5550
	1    0    0    -1  
$EndComp
Text HLabel 14500 4250 2    60   Input ~ 0
RxD
Text HLabel 14500 4150 2    60   Input ~ 0
TxD
Text HLabel 14500 4050 2    60   Input ~ 0
DD
Text HLabel 2300 5450 0    60   Input ~ 0
CMD
Text HLabel 2300 6850 0    60   Input ~ 0
CLK
Text HLabel 2300 6450 0    60   Input ~ 0
D0
Text HLabel 2300 6550 0    60   Input ~ 0
D1
Text HLabel 2300 6650 0    60   Input ~ 0
D2
Text HLabel 2300 6750 0    60   Input ~ 0
D3
Text HLabel 14500 6050 2    60   Input ~ 0
SD_DET
Text HLabel 14500 5050 2    60   Input ~ 0
RF_Tx_EN
Text HLabel 2300 5850 0    60   Input ~ 0
RF_MISO
Text HLabel 2300 5950 0    60   Input ~ 0
RF_MOSI
Text HLabel 14500 6650 2    60   Input ~ 0
RF_SCK
Text HLabel 14500 4850 2    60   Input ~ 0
RF_IRQ
Text HLabel 14500 6550 2    60   Input ~ 0
CAN1TX
Text HLabel 14500 6450 2    60   Input ~ 0
CAN1RX
$Comp
L power:GND #PWR033
U 1 1 5A7DE5F9
P 8200 7550
F 0 "#PWR033" H 8200 7300 50  0001 C CNN
F 1 "GND" H 8200 7400 50  0000 C CNN
F 2 "" H 8200 7550 50  0001 C CNN
F 3 "" H 8200 7550 50  0001 C CNN
	1    8200 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 7550 8300 7550
Connection ~ 8300 7550
Connection ~ 8400 7550
Connection ~ 8500 7550
$Comp
L pqws-main-hw-rescue:C_Small-device C25
U 1 1 5A7DE7BC
P 7800 3050
F 0 "C25" H 7810 3120 50  0000 L CNN
F 1 "100nF" H 7810 2970 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7800 3050 50  0001 C CNN
F 3 "" H 7800 3050 50  0001 C CNN
	1    7800 3050
	1    0    0    -1  
$EndComp
$Comp
L pqws-main-hw-rescue:CP1_Small-device C22
U 1 1 5A7DE85A
P 6900 3050
F 0 "C22" H 6910 3120 50  0000 L CNN
F 1 "4.7uF" H 6910 2970 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 6900 3050 50  0001 C CNN
F 3 "" H 6900 3050 50  0001 C CNN
	1    6900 3050
	1    0    0    -1  
$EndComp
$Comp
L pqws-main-hw-rescue:CP1_Small-device C27
U 1 1 5A7DE885
P 8900 2400
F 0 "C27" H 8910 2470 50  0000 L CNN
F 1 "1uF" H 8910 2320 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 8900 2400 50  0001 C CNN
F 3 "" H 8900 2400 50  0001 C CNN
	1    8900 2400
	1    0    0    -1  
$EndComp
$Comp
L pqws-main-hw-rescue:C_Small-device C26
U 1 1 5A7DE90E
P 8650 2400
F 0 "C26" H 8660 2470 50  0000 L CNN
F 1 "10nF" H 8660 2320 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 8650 2400 50  0001 C CNN
F 3 "" H 8650 2400 50  0001 C CNN
	1    8650 2400
	1    0    0    -1  
$EndComp
$Comp
L pqws-main-hw-rescue:C_Small-device C24
U 1 1 5A7DE939
P 7500 3050
F 0 "C24" H 7510 3120 50  0000 L CNN
F 1 "100nF" H 7510 2970 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7500 3050 50  0001 C CNN
F 3 "" H 7500 3050 50  0001 C CNN
	1    7500 3050
	1    0    0    -1  
$EndComp
$Comp
L pqws-main-hw-rescue:C_Small-device C23
U 1 1 5A7DE985
P 7200 3050
F 0 "C23" H 7210 3120 50  0000 L CNN
F 1 "100nF" H 7210 2970 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad0.84x1.00mm_HandSolder" H 7200 3050 50  0001 C CNN
F 3 "" H 7200 3050 50  0001 C CNN
	1    7200 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR034
U 1 1 5A7DEA89
P 8650 2550
F 0 "#PWR034" H 8650 2300 50  0001 C CNN
F 1 "GND" H 8650 2400 50  0000 C CNN
F 2 "" H 8650 2550 50  0001 C CNN
F 3 "" H 8650 2550 50  0001 C CNN
	1    8650 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 3450 8500 2250
Wire Wire Line
	8500 2250 8650 2250
Wire Wire Line
	8650 2550 8650 2500
Wire Wire Line
	8900 2500 8900 2550
Wire Wire Line
	8900 2550 8650 2550
Connection ~ 8650 2250
Wire Wire Line
	8650 2250 8650 2300
Wire Wire Line
	8900 2250 8900 2300
Wire Wire Line
	7800 2950 7800 2850
Wire Wire Line
	6900 2850 7200 2850
Wire Wire Line
	8200 2850 8200 3450
Wire Wire Line
	8300 2850 8300 3450
Connection ~ 8200 2850
Wire Wire Line
	8400 2850 8400 3450
Connection ~ 8300 2850
Wire Wire Line
	7500 2850 7500 2950
Connection ~ 7800 2850
Wire Wire Line
	7200 2850 7200 2950
Connection ~ 7500 2850
Wire Wire Line
	6900 2850 6900 2950
Connection ~ 7200 2850
Wire Wire Line
	6900 3150 6900 3300
Wire Wire Line
	6900 3300 7200 3300
Wire Wire Line
	7800 3300 7800 3150
Wire Wire Line
	7500 3150 7500 3300
Connection ~ 7500 3300
Wire Wire Line
	7200 3150 7200 3300
Connection ~ 7200 3300
$Comp
L power:GND #PWR035
U 1 1 5A7DED74
P 6900 3300
F 0 "#PWR035" H 6900 3050 50  0001 C CNN
F 1 "GND" H 6900 3150 50  0000 C CNN
F 2 "" H 6900 3300 50  0001 C CNN
F 3 "" H 6900 3300 50  0001 C CNN
	1    6900 3300
	1    0    0    -1  
$EndComp
$Comp
L pqws-main-hw-rescue:Battery_Cell-device BT1
U 1 1 5A7DF13D
P 1850 4600
F 0 "BT1" H 1950 4700 50  0000 L CNN
F 1 "Battery_Cell" H 1950 4600 50  0000 L CNN
F 2 "" V 1850 4660 50  0001 C CNN
F 3 "" V 1850 4660 50  0001 C CNN
	1    1850 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR037
U 1 1 5A7DF1A6
P 1850 4750
F 0 "#PWR037" H 1850 4500 50  0001 C CNN
F 1 "GND" H 1850 4600 50  0000 C CNN
F 2 "" H 1850 4750 50  0001 C CNN
F 3 "" H 1850 4750 50  0001 C CNN
	1    1850 4750
	1    0    0    -1  
$EndComp
Text HLabel 14500 6750 2    60   Input ~ 0
CAN1_EN
Wire Wire Line
	8300 7550 8400 7550
Wire Wire Line
	8400 7550 8500 7550
Wire Wire Line
	8500 7550 8600 7550
Wire Wire Line
	8650 2250 8900 2250
Wire Wire Line
	8200 2850 8300 2850
Wire Wire Line
	8300 2850 8400 2850
Wire Wire Line
	7800 2850 8200 2850
Wire Wire Line
	7500 2850 7800 2850
Wire Wire Line
	7200 2850 7500 2850
Wire Wire Line
	7500 3300 7800 3300
Wire Wire Line
	7200 3300 7500 3300
NoConn ~ 2300 6950
NoConn ~ 2300 7050
NoConn ~ 2300 7150
NoConn ~ 2300 6350
NoConn ~ 2300 6250
NoConn ~ 2300 6150
NoConn ~ 2300 6050
NoConn ~ 2300 5750
NoConn ~ 2300 5700
NoConn ~ 2300 5150
NoConn ~ 2300 5250
NoConn ~ 2300 3950
NoConn ~ 2300 4150
NoConn ~ 14500 3950
NoConn ~ 14500 4350
NoConn ~ 14500 4450
NoConn ~ 14500 4550
NoConn ~ 14500 4650
NoConn ~ 14500 4750
NoConn ~ 14500 4950
NoConn ~ 14500 5150
NoConn ~ 14500 5250
NoConn ~ 14500 5350
NoConn ~ 14500 5450
NoConn ~ 14500 5650
NoConn ~ 14500 5750
NoConn ~ 14500 5850
NoConn ~ 14500 6150
NoConn ~ 14500 6250
NoConn ~ 14500 6350
NoConn ~ 14500 6850
NoConn ~ 14500 6950
NoConn ~ 14500 7050
NoConn ~ 14500 7150
NoConn ~ 2300 5650
$Comp
L power:VCC #PWR032
U 1 1 5A8675BB
P 8650 2250
F 0 "#PWR032" H 8650 2100 50  0001 C CNN
F 1 "VCC" H 8667 2423 50  0000 C CNN
F 2 "" H 8650 2250 50  0001 C CNN
F 3 "" H 8650 2250 50  0001 C CNN
	1    8650 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 4400 1850 4350
Wire Wire Line
	1850 4700 1850 4750
$Comp
L power:VCC #PWR036
U 1 1 5A868823
P 6900 2850
F 0 "#PWR036" H 6900 2700 50  0001 C CNN
F 1 "VCC" H 6917 3023 50  0000 C CNN
F 2 "" H 6900 2850 50  0001 C CNN
F 3 "" H 6900 2850 50  0001 C CNN
	1    6900 2850
	1    0    0    -1  
$EndComp
Connection ~ 6900 2850
Connection ~ 8650 2550
Wire Wire Line
	8600 3450 8600 2850
Wire Wire Line
	8600 2850 8400 2850
Connection ~ 8400 2850
$Comp
L power:+BATT #PWR041
U 1 1 5A86D1C4
P 1850 4350
F 0 "#PWR041" H 1850 4200 50  0001 C CNN
F 1 "+BATT" H 1865 4523 50  0000 C CNN
F 2 "" H 1850 4350 50  0001 C CNN
F 3 "" H 1850 4350 50  0001 C CNN
	1    1850 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 4350 1850 4350
Connection ~ 1850 4350
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5A874783
P 1850 4350
F 0 "#FLG01" H 1850 4425 50  0001 C CNN
F 1 "PWR_FLAG" V 1850 4478 50  0000 L CNN
F 2 "" H 1850 4350 50  0001 C CNN
F 3 "" H 1850 4350 50  0001 C CNN
	1    1850 4350
	0    -1   -1   0   
$EndComp
Text HLabel 14500 5950 2    50   Input ~ 0
RST_TRIG
$EndSCHEMATC
